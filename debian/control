Source: gwenview
Section: kde
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sune Vuorela <sune@debian.org>, Maximiliano Curia <maxy@debian.org>
Build-Depends: baloo-kf5-dev (>= 5.13) [linux-any],
               cmake (>= 2.8.12),
               debhelper (>= 11~),
               extra-cmake-modules (>= 1.7.0~),
               kinit-dev,
               libcfitsio-dev,
               libexiv2-dev,
               libjpeg-dev,
               libkf5activities-dev (>= 5.25~),
               libkf5doctools-dev (>= 5.25~),
               libkf5i18n-dev (>= 5.25~),
               libkf5iconthemes-dev (>= 5.25~),
               libkf5itemmodels-dev (>= 5.25~),
               libkf5kdcraw-dev,
               libkf5kio-dev (>= 5.25~),
               libkf5kipi-dev,
               libkf5notifications-dev (>= 5.25~),
               libkf5parts-dev (>= 5.25~),
               libkf5windowsystem-dev (>= 5.25~),
               liblcms2-dev,
               libphonon4qt5-dev (>= 4.6.60),
               libphonon4qt5experimental-dev (>= 4.9.1),
               libpng-dev,
               libqt5opengl5-dev (>= 5.6.0~),
               libqt5svg5-dev (>= 5.6.0~),
               libqt5x11extras5-dev (>= 5.6.0~),
               pkg-config,
               pkg-kde-tools (>> 0.15.15),
               qtbase5-dev (>= 5.6.0~),
               qtscript5-dev (>= 5.4),
Standards-Version: 4.1.4
Homepage: http://www.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/gwenview
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/gwenview.git

Package: gwenview
Section: graphics
Architecture: any
Depends: kinit, ${misc:Depends}, ${shlibs:Depends}
Recommends: kamera, kio-extras, qt5-image-formats-plugins
Breaks: ${kde-l10n:all}
Replaces: ${kde-l10n:all}
Description: image viewer
 Gwenview is an image viewer, ideal for browsing and displaying a collection of
 images.  It is capable of showing images in a full-screen slideshow view and
 making simple adjustments, such as rotating or cropping images.
 .
 This package is part of the KDE graphics module.
